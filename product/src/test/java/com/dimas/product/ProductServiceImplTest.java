package com.dimas.product;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dimas.product.dto.request.ProductRequestDto;
import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductResponseDto;
import com.dimas.product.model.Product;
import com.dimas.product.model.ProductCategory;
import com.dimas.product.repository.ProductRepository;
import com.dimas.product.service.ProductCategoryService;
import com.dimas.product.service.impl.ProductServiceImpl;

public class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductCategoryService productCategoryService;

    @InjectMocks
    private ProductServiceImpl productService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindByCode() {
        String code = "code1";

        ProductCategory category = new ProductCategory();
        category.setId("1");
        category.setCategory("Category 1");
        
        Product product = new Product(code, "Product 1", category, "Brand 1", "Type 1", "Description 1", LocalDateTime.now(), LocalDateTime.now());

		when(productRepository.findById(code)).thenReturn(Optional.of(product ));

        DataResponse<ProductResponseDto> response = productService.findByCode(code);

        assertTrue(response.getStatus());
        assertEquals("Product 1", response.getData().getName());

        verify(productRepository, times(1)).findById(code);
    }

    @Test
    public void testSave() {
        String code = "code1";
        String name = "Product 1";
        String categoryId = "1";
        String brand = "Brand 1";
        String type = "Type 1";
        String description = "Description 1";

        ProductRequestDto request = new ProductRequestDto();
        request.setCode(code);
        request.setName(name);
        request.setCategory(categoryId);
        request.setBrand(brand);
        request.setType(type);
        request.setDescription(description);

        ProductCategory category = new ProductCategory();
        category.setId(categoryId);
        category.setCategory("Category 1");
        
        when(productCategoryService.findProductCategoryById(categoryId)).thenReturn(category);

        DataResponse<ProductResponseDto> response = productService.save(request);

        assertTrue(response.getStatus());
        assertEquals(code, response.getData().getCode());
        assertEquals(name, response.getData().getName());
        assertEquals(category.getCategory(), response.getData().getCategory());
        assertEquals(brand, response.getData().getBrand());
        assertEquals(type, response.getData().getType());
        assertEquals(description, response.getData().getDescription());

        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void testEdit() {
        String code = "code1";
        String name = "Product 1 Updated";
        String categoryId = "1";
        String brand = "Brand 1 Updated";
        String type = "Type 1 Updated";
        String description = "Description 1 Updated";

        ProductRequestDto request = new ProductRequestDto();
        request.setCode(code);
        request.setName(name);
        request.setCategory(categoryId);
        request.setBrand(brand);
        request.setType(type);
        request.setDescription(description);
        
        ProductCategory category = new ProductCategory();
        category.setId(categoryId);

        when(productCategoryService.findProductCategoryById(categoryId)).thenReturn(category);

        Product existingProduct = new Product(code, "Product 1", category, "Brand 1", "Type 1", "Description 1", LocalDateTime.now(), LocalDateTime.now());

        when(productRepository.findById(code)).thenReturn(Optional.of(existingProduct));

        DataResponse<ProductResponseDto> response = productService.edit(request);

        assertTrue(response.getStatus());
        assertEquals(name, response.getData().getName());
        assertEquals(brand, response.getData().getBrand());
        assertEquals(type, response.getData().getType());
        assertEquals(description, response.getData().getDescription());

        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void testDeleteByCode() {
        String code = "1";

        DataResponse<ProductResponseDto> response = productService.deleteByCode(code);

        assertTrue(response.getStatus());
        assertNull(response.getData());

        verify(productRepository, times(1)).deleteById(code);
    }
}
