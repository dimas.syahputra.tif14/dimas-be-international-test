INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('1', 'Product 1', '1', 'Brand 1', 'Type 1', 'Description 1', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('2', 'Product 2', '2', 'Brand 2', 'Type 2', 'Description 2', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('3', 'Product 3', '1', 'Brand 3', 'Type 3', 'Description 3', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('4', 'Product 4', '2', 'Brand 4', 'Type 4', 'Description 4', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('5', 'Product 5', '1', 'Brand 5', 'Type 5', 'Description 5', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('6', 'Product 6', '2', 'Brand 6', 'Type 6', 'Description 6', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('7', 'Product 7', '1', 'Brand 7', 'Type 7', 'Description 7', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('8', 'Product 8', '2', 'Brand 8', 'Type 8', 'Description 8', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('9', 'Product 9', '1', 'Brand 9', 'Type 9', 'Description 9', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('10', 'Product 10', '2', 'Brand 10', 'Type 10', 'Description 10', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('11', 'Product 11', '1', 'Brand 11', 'Type 11', 'Description 11', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('12', 'Product 12', '2', 'Brand 12', 'Type 12', 'Description 12', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('13', 'Product 13', '1', 'Brand 13', 'Type 13', 'Description 13', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('14', 'Product 14', '2', 'Brand 14', 'Type 14', 'Description 14', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('15', 'Product 15', '1', 'Brand 15', 'Type 15', 'Description 15', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('16', 'Product 16', '2', 'Brand 16', 'Type 16', 'Description 16', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('17', 'Product 17', '1', 'Brand 17', 'Type 17', 'Description 17', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT (CODE, NAME, CATEGORY_ID, BRAND, "TYPE", DESCRIPTION, CREATED_AT, UPDATED_AT)
VALUES ('18', 'Product 18', '2', 'Brand 18', 'Type 18', 'Description 18', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO PRODUCT_CATEGORY (ID, CATEGORY)
VALUES ('1', 'Category 1');

INSERT INTO PRODUCT_CATEGORY (ID, CATEGORY)
VALUES ('2', 'Category 2');