package com.dimas.product.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dimas.product.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
	
	List<Product> findAllByOrderByUpdatedAtDesc(Pageable pageable);

}
