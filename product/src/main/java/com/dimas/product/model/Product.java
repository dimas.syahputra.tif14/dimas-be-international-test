package com.dimas.product.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT")
public class Product {
	
	@Id
	@Column(name = "CODE")
	private String code;
		
	@Column(name = "NAME")
	private String name;
		
	@ManyToOne
    @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID")
    private ProductCategory category;
	
	@Column(name = "BRAND")
	private String brand;
	
	@Column(name = "TYPE")
	private String type;
	
	@Lob
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "CREATED_AT")
	private LocalDateTime createdAt;
	
	@Column(name = "UPDATED_AT")
	private LocalDateTime updatedAt;
	
}
