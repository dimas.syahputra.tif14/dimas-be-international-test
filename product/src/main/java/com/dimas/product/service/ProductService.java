package com.dimas.product.service;

import java.util.List;

import com.dimas.product.dto.request.ProductRequestDto;
import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductResponseDto;

public interface ProductService {
	
	public DataResponse<List<ProductResponseDto>> findByPage(String page);
	
	public DataResponse<List<ProductResponseDto>> findByAll();
	
	public DataResponse<ProductResponseDto> findByCode(String code);
	
	public DataResponse<ProductResponseDto> save(ProductRequestDto request);
	
	public DataResponse<ProductResponseDto> edit(ProductRequestDto request);
	
	public DataResponse<ProductResponseDto> deleteByCode(String code);

}
