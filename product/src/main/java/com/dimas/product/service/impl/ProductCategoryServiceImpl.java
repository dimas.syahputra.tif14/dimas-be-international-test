package com.dimas.product.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductCategoryResponseDto;
import com.dimas.product.model.ProductCategory;
import com.dimas.product.repository.ProductCategoryRepository;
import com.dimas.product.service.ProductCategoryService;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
	
	@Autowired
	private ProductCategoryRepository repository;

	@Override
	public DataResponse<List<ProductCategoryResponseDto>> findAll() {
		
		List<ProductCategoryResponseDto> response = new ArrayList<>();
		
		try {
			
			repository.findAll()
			.forEach(pc -> response.add(new ProductCategoryResponseDto(pc.getId(), pc.getCategory())));
			
			return new DataResponse<List<ProductCategoryResponseDto>>(true, "Success", response);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<List<ProductCategoryResponseDto>>(false, e.getMessage(), response);
		}
	}

	@Override
	public ProductCategory findProductCategoryById(String id) {
		return repository.findById(id).get();
	}

}
