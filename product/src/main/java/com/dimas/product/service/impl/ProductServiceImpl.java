package com.dimas.product.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dimas.product.dto.request.ProductRequestDto;
import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductResponseDto;
import com.dimas.product.model.Product;
import com.dimas.product.model.ProductCategory;
import com.dimas.product.repository.ProductRepository;
import com.dimas.product.service.ProductCategoryService;
import com.dimas.product.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository repository;
	
	@Autowired
	private ProductCategoryService productCategoryService;
	
	private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMMM yy HH:mm:ss");

	@Override
	public DataResponse<List<ProductResponseDto>> findByPage(String page) {
		
		List<ProductResponseDto> response = new ArrayList<>();
		
		try {
			
			Integer pg = 0;
			Integer maxPg = 10;
			
			if(!StringUtils.hasText(page)) {
				pg = 0;
			} else {
				pg = Integer.valueOf(page);
			}
			
			Pageable pageable = PageRequest.of(pg - 1, maxPg);

			repository.findAllByOrderByUpdatedAtDesc(pageable)
				.forEach(p -> response.add(new ProductResponseDto(
						p.getCode(), 
						p.getName(), 
						p.getCategory().getCategory(), 
						p.getBrand(), 
						p.getType(), 
						p.getDescription(), 
						p.getCreatedAt().format(dateFormatter), 
						p.getUpdatedAt().format(dateFormatter))));
			
			Long totalPage = repository.count() / maxPg;
			
			if(repository.count() % maxPg > 0) {
				totalPage++;
			}
			
			return new DataResponse<List<ProductResponseDto>>(true, "Success", Integer.valueOf(page), totalPage, response);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<List<ProductResponseDto>>(false, e.getMessage(), 0, 0L, response);
		}
		
	}
	
	@Override
	public DataResponse<List<ProductResponseDto>> findByAll() {
		
		List<ProductResponseDto> response = new ArrayList<>();
		
		try {

			repository.findAll()
				.forEach(p -> response.add(new ProductResponseDto(
						p.getCode(), 
						p.getName(), 
						p.getCategory().getCategory(), 
						p.getBrand(), 
						p.getType(), 
						p.getDescription(), 
						p.getCreatedAt().format(dateFormatter), 
						p.getUpdatedAt().format(dateFormatter))));
			
			return new DataResponse<List<ProductResponseDto>>(true, "Success", response);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<List<ProductResponseDto>>(false, e.getMessage(), response);
		}
		
	}

	@Override
	public DataResponse<ProductResponseDto> findByCode(String code) {

		ProductResponseDto response = new ProductResponseDto();
		
		try {
			
			Optional<Product> product = repository.findById(code);
		
			if(product.isPresent()) {
				
				Product p = product.get();
				
				response = new ProductResponseDto(
						p.getCode(), 
						p.getName(), 
						p.getCategory().getId(), 
						p.getBrand(), 
						p.getType(), 
						p.getDescription(), 
						p.getCreatedAt().format(dateFormatter), 
						p.getUpdatedAt().format(dateFormatter));
				
			}
			
		
			return new DataResponse<ProductResponseDto>(true, "Success", response);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<ProductResponseDto>(false, e.getMessage(), response);
		}
	}

	@Override
	public DataResponse<ProductResponseDto> save(ProductRequestDto request) {
		
		ProductResponseDto response = new ProductResponseDto();
		
		try {
			
			ProductCategory category = productCategoryService.findProductCategoryById(request.getCategory());
			
			Product p = new Product(
					request.getCode(), 
					request.getName(), 
					category, 
					request.getBrand(), 
					request.getType(), 
					request.getDescription(), 
					LocalDateTime.now(), 
					LocalDateTime.now());

			repository.save(p);
			
			response = new ProductResponseDto(
					p.getCode(), 
					p.getName(), 
					p.getCategory().getCategory(), 
					p.getBrand(), 
					p.getType(), 
					p.getDescription(), 
					p.getCreatedAt().format(dateFormatter), 
					p.getUpdatedAt().format(dateFormatter));
		
			return new DataResponse<ProductResponseDto>(true, "Success", response);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<ProductResponseDto>(false, e.getMessage(), response);
		}
	}

	@Override
	public DataResponse<ProductResponseDto> edit(ProductRequestDto request) {
		ProductResponseDto response = new ProductResponseDto();
		
		try {
			ObjectMapper obm = new ObjectMapper();
			System.out.println(obm.writeValueAsString(request));
			Optional<Product> product = repository.findById(request.getCode());
		
			if(product.isPresent()) {
				System.out.println("product is present");
				Product p = product.get();
				
				p.setName(request.getName());
				
				ProductCategory category = productCategoryService.findProductCategoryById(request.getCategory());
				
				p.setCategory(category);
				p.setBrand(request.getBrand());
				p.setType(request.getType());
				p.setDescription(request.getDescription());
				p.setUpdatedAt(LocalDateTime.now());
				
				repository.save(p);
				
				response = new ProductResponseDto(
						p.getCode(), 
						p.getName(), 
						p.getCategory().getCategory(), 
						p.getBrand(), 
						p.getType(), 
						p.getDescription(), 
						p.getCreatedAt().format(dateFormatter), 
						p.getUpdatedAt().format(dateFormatter));
				
			}
			
			return new DataResponse<ProductResponseDto>(true, "Success", response);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<ProductResponseDto>(false, e.getMessage(), response);
		}
	}

	@Override
	public DataResponse<ProductResponseDto> deleteByCode(String code) {
		
		try {
			repository.deleteById(code);
			return new DataResponse<ProductResponseDto>(true, "Success", null);
		} catch (Exception e) {
			e.printStackTrace();
			return new DataResponse<ProductResponseDto>(false, e.getMessage(), null);
		}
	}

}
