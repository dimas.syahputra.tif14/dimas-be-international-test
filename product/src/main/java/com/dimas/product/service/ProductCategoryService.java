package com.dimas.product.service;

import java.util.List;

import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductCategoryResponseDto;
import com.dimas.product.model.ProductCategory;

public interface ProductCategoryService {
	
	public DataResponse<List<ProductCategoryResponseDto>> findAll();

	public ProductCategory findProductCategoryById(String id);
}
