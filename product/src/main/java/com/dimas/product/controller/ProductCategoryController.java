package com.dimas.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductCategoryResponseDto;
import com.dimas.product.service.ProductCategoryService;

@RestController
@RequestMapping("/api/product-categories")
@CrossOrigin(origins = "*")
public class ProductCategoryController {
	
	@Autowired
	private ProductCategoryService service;
	
	@GetMapping
    public DataResponse<List<ProductCategoryResponseDto>> getAllProductCategories() {
        return service.findAll();
    }

}
