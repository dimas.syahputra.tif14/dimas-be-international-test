package com.dimas.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dimas.product.dto.request.ProductRequestDto;
import com.dimas.product.dto.response.DataResponse;
import com.dimas.product.dto.response.ProductResponseDto;
import com.dimas.product.service.ProductService;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {

    @Autowired
    private ProductService service;
    
    @GetMapping
    public DataResponse<List<ProductResponseDto>> getAllProducts() {
        return service.findByAll();
    }

    @GetMapping("/pagination")
    public DataResponse<List<ProductResponseDto>> getPaginationProducts(@RequestParam String page) {
        return service.findByPage(page);
    }

    @GetMapping("/{code}")
    public DataResponse<ProductResponseDto> getProductByCode(@PathVariable String code) {
        return service.findByCode(code);
    }

    @PostMapping
    public DataResponse<ProductResponseDto> createProduct(@RequestBody ProductRequestDto requestDto) {
        return service.save(requestDto);
    }

    @PutMapping("/{code}")
    public DataResponse<ProductResponseDto> updateProduct(@PathVariable String code, @RequestBody ProductRequestDto requestDto) {
        requestDto.setCode(code);
        return service.edit(requestDto);
    }

    @DeleteMapping("/{code}")
    public DataResponse<ProductResponseDto> deleteProduct(@PathVariable String code) {
        return service.deleteByCode(code);
    }
}
