package com.dimas.product.dto.request;

import lombok.Data;

@Data
public class ProductRequestDto {
	
	private String code;
		
	private String name;
	
	private String category;
	
	private String brand;
	
	private String type;

	private String description;

}
