package com.dimas.product.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponseDto {

	private String code;
		
	private String name;
	
	private String category;
	
	private String brand;
	
	private String type;
	
	private String description;
	
	private String createdAt;
	
	private String updatedAt;
	
}
