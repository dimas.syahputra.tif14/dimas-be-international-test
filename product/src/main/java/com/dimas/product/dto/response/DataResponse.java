package com.dimas.product.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataResponse<T> {
	
	private Boolean status;
    private String message;
    private Integer page;
    private Long totalPage;
    private T data;
	public DataResponse(Boolean status, String message, T data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

}
