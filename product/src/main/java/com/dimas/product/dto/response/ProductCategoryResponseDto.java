package com.dimas.product.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductCategoryResponseDto {
	
	private String id;
		
	private String category;
	
}