# DIMAS BE INTERNATIONAL TEST

## Getting started

## Name
DIMAS BE INTERNATIONAL TEST

## HOW TO RUN
Follow sequence below :
1. Check USER GUIDE ENVIRONMENT SETUP.docx
2. Check USER GUIDE RUN APPLICATION.docx

## Description
CRUD PRODUCT WITH BOOTSTRAP
1. API DOCUMENTATION	: http://localhost:8080/swagger-ui/index.html
2. H2 CONSOLE 		  	: http://localhost:8080/h2-console/

## Authors and acknowledgment
Made By DIMAS SYAHPUTRA